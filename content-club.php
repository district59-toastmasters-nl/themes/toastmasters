<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package GeneratePress
 */
 
// No direct access, please
if ( ! defined( 'ABSPATH' ) ) exit;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php generate_article_schema( 'CreativeWork' ); ?>>
	<div class="inside-article">
		<?php do_action( 'generate_before_content'); ?>
		
		<?php if ( generate_show_title() ) : ?>
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>' ); ?>
			</header><!-- .entry-header -->
		<?php endif; ?>
		
		<?php do_action( 'generate_after_entry_header'); ?>
		<div class="entry-content" itemprop="text">
<div class="club-informatie">
<table>
<tbody>
<tr>
<td>Voertaal</td>
<td><?php the_field('voertaal'); ?></td>
</tr>
<tr>
<td>Clubdagen</td>
<td><?php the_field('clubdagen'); ?></td>
</tr>
<tr>
<td>Tijden</td>
<td><?php the_field('tijden'); ?></td>
</tr>
<tr>
<td>Locatie</td>
<td><?php the_field('locatie'); ?></td>
</tr>
<tr>
<td>TM&nbsp;Site</td>
<td>
<?php if( get_field('clubwebsite') ): ?>
	
	<a href="<?php the_field('clubwebsite'); ?>" target="_blank" rel="noopener"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/easyspeak.png" alt="EasySpeak" title="EasySpeak" width="61" height="20" /></a>
	
<?php endif; ?>
</td>
</tr>
<tr>
<td>Social&nbsp;Media</td>
<td>
<?php if( get_field('facebook') ): ?>
	
	<a href="<?php the_field('facebook'); ?>" title="Facebook" target="_blank" rel="noopener"><i class="fa fa-facebook-official fa-lg" aria-hidden="true"></i></a>
	
<?php endif; ?>
<?php if( get_field('meetup') ): ?>
	
	<a href="<?php the_field('meetup'); ?>" title="Meetup"  target="_blank" rel="noopener"><i class="fa fa-meetup fa-lg" aria-hidden="true"></i></a>
	
<?php endif; ?>
<?php if( get_field('linkedin') ): ?>
	
	<a href="<?php the_field('linkedin'); ?>" title="LinkedIn"  target="_blank" rel="noopener"><i class="fa fa-linkedin-square  fa-lg" aria-hidden="true"></i></a>
	
<?php endif; ?>
<?php if( get_field('twitter') ): ?>
	
	<a href="<?php the_field('twitter'); ?>" title="Twitter"  target="_blank" rel="noopener"><i class="fa fa fa-twitter  fa-lg" aria-hidden="true"></i></a>
	
<?php endif; ?>
<?php if( get_field('youtube') ): ?>
	
	<a href="<?php the_field('youtube'); ?>" title="YouTube"  target="_blank" rel="noopener"><i class="fa fa-youtube fa-lg" aria-hidden="true"></i></a>
	
<?php endif; ?>
<?php if( get_field('instagram') ): ?>
	
	<a href="<?php the_field('instagram'); ?>" title="Instagram"  target="_blank" rel="noopener"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
	
<?php endif; ?>
</td>
</tbody>
</table>
</div>			<?php the_content(); ?>
			<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'generatepress' ),
				'after'  => '</div>',
			) );
			?>
		</div><!-- .entry-content -->
		<?php do_action( 'generate_after_content'); ?>
	</div><!-- .inside-article -->
</article><!-- #post-## -->
